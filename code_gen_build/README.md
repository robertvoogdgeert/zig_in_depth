# Zig Build System Code Gen Example

In this example Zig project, there's [gen.zig](src/gen.zig) that uses a command line
argument passed in from the build system as the path for the Zig file it will
generate. This command line argument is set up in [build.zig](build.zig), specifically in 
the `run_gen_exe` step with `addOutputFileArg`. By generating the file with this
path, it will be cached by the build system and if [gen.zig](src/gen.zig) changes, it will
be re-generated anew. If [foo.zig](src/foo.zig) is modified, it will be reset with the cached
version on the next `zig build run`.

So if you need to generated Zig source code and want to take advantage of the build system's
powerful caching, this is a way to do it.
